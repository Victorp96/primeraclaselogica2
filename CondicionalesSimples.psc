Algoritmo sin_titulo
	//Declaracion de variables
	Definir lapso1, lapso2, lapso3, promedio Como Real
	Definir nro_estudiantes como entero
	
	Mostrar "Ingrese la cantidad de estudiantes a procesar"
	Leer nro_estudiantes 

	para i=1 hasta nro_estudiantes
		
		Mostrar "Ingrese las notas del " nro_estudiantes " estudiante"
		Mostrar "Ingrese la nota del primer lapso: " Sin Saltar
		Leer lapso1
		
		Mostrar "Ingrese la nota del segundo lapso: " Sin Saltar
		Leer lapso2
		
		Mostrar "Ingrese la nota del tercer lapso: " Sin Saltar
		Leer lapso3
		
		//Bloque de procesos
		promedio=(lapso1+lapso2+lapso3)/3
		Mostrar ""
		si promedio>= 10 Entonces
			Mostrar "Aprobado"
		FinSi
		si promedio< 10 Entonces
			Mostrar "Reprobado"
		FinSi
		
		si promedio>=19 Entonces
			Mostrar "Felicitaciones!! aprobaste con: " promedio " ptos. Sigue asi!"
		FinSi
		
		si promedio<5
			Mostrar "Reprobado...Debe esforzarse mas"
		FinSi
		
		//Bloque de salidas
		Mostrar "Tu promedio es de : " promedio " ptos"
	FinPara
	
	
	//Mostrar Mensaje1="" blanquear mensajes
FinAlgoritmo
